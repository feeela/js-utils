/**
 * TabAccordion – An accordion, that may change its display to tabbed content based on the screen width.
 *
 * @author    Thomas Heuer <projekte@thomas-heuer.eu>
 *
 * Some nice extra ideas:
 * @todo Measure & set tab content heights individually and remove parent min-height to avoid gaps below tabbed-content
 * @todo allow nested accordions and open ancestor if an sub-accordion item was opened via a has in the URL
 */
import * as MediaQuerySync from "./view.media-query-sync.js";
import {debounce, extend} from "./helper.js";

/**
 * This prefix is used for additional object properties created on existing types (i.e. HTMLElement)
 * @type {String}
 */
const MODULE_PREFIX = 'tabAccordion';

/**
 * Display constants.
 *
 * @type {Object}
 */
const DISPLAY = Object.create(Object.prototype, {
    none: {value: 0, enumerable: true},
    accordion: {value: 1, enumerable: true},
    tabs: {value: 2, enumerable: true}
});

/**
 * The TabAccordion behavior-types, that define how to display a group of sections on a
 * specified media query breakpoint.
 *
 * New types may be created according to the pattern below, where:
 * - "type" is a chosen identifier to be set in the data-attribute
 * - "breakpoint" is one of the breakpoints defined in the MediaQuerySync.DEVICE_STATE_MAP
 * - "display" is one of the DISPLAY constants
 *
 * Make sure the breakpoints for each type do not overlap. (e.g. 'md' must only occur once per type)
 *
 * @type {Object} { type: { breakpoint: display } }
 */
const BEHAVIOR_TYPES = {
    accordion: {
        xxs: DISPLAY.accordion,
        xs: DISPLAY.accordion,
        sm: DISPLAY.accordion,
        md: DISPLAY.accordion,
        lg: DISPLAY.accordion,
        xl: DISPLAY.accordion
    },
    mobileOnlyAccordion: {
        xxs: DISPLAY.accordion,
        xs: DISPLAY.accordion,
        sm: DISPLAY.accordion,
        md: DISPLAY.none,
        lg: DISPLAY.none,
        xl: DISPLAY.none
    },
    tabs: {
        xxs: DISPLAY.tabs,
        xs: DISPLAY.tabs,
        sm: DISPLAY.tabs,
        md: DISPLAY.tabs,
        lg: DISPLAY.tabs,
        xl: DISPLAY.tabs
    }
};
/* Add the default value as read-only property */
Object.defineProperty(BEHAVIOR_TYPES, 'tabAccordion', {
    value: Object.create(Object.prototype, {
        xxs: {value: DISPLAY.accordion, enumerable: true},
        xs: {value: DISPLAY.accordion, enumerable: true},
        sm: {value: DISPLAY.accordion, enumerable: true},
        md: {value: DISPLAY.tabs, enumerable: true},
        lg: {value: DISPLAY.tabs, enumerable: true},
        xl: {value: DISPLAY.tabs, enumerable: true}
    }),
    enumerable: true
});


/**
 * Configuration; may get overwritten with user provided configuration.
 *
 * @type {Object}
 */
const DEFAULT_CONFIG = {

    /**
     * The selector entries must contain valid CSS selectors to be used in querySelector()
     * @type {{wrapper: String, section: String, sectionHeading: String, sectionBody: String}}
     */
    selector: {

        /**
         * A CSS selector to identify an area that should be used as an accordion or as tabbed content.
         *
         * Should yield: Element
         */
        wrapper: '[data-tabaccordion]',

        /**
         * This is a single part/section of the TabAccordion.
         * The attribute `data-tabaccordion-section` is set during initialization to identify existing
         * sections and prevent duplicated wrapping of sections of nested TabAccordions.
         *
         * Constraint: must be descendants of `selector.wrapper`
         * Should yield: NodeList
         */
        section: 'section:not([data-tabaccordion-section])',

        /**
         * This is the section header – the part which to click on to open a section.
         *
         * Constraint: must be a descendant of `selector.section`
         * Should yield: Element
         */
        sectionHeading: ':scope > header',

        /**
         * This is a selector to find section contents.
         * You may also set a single, specific body wrapper element selector here.
         *
         * Constraint: must be descendants of `selector.section`
         * Should yield: NodeList
         */
        sectionBody: ':scope > *:not(header)'
    },

    /**
     * This class name will be added to a new element that is inserted around the section body elements
     */
    sectionBodyWrapperClass: 'tabaccordion-body-wrapper',

    /**
     * A class name to designate opened sections
     * @type {String}
     */
    showSectionClass: 'open',

    /**
     * A function to determine which section of a TabAccordion to open on load.
     * Returns either the actual section element or a numeric index.
     *
     * @this Accordion
     * @return {Element|Number}
     */
    getInitinalSectionToOpen: function () {
        let groupOpen = 0;

        /* Open a specified accordion element if specified by data attribute, e.g. 'data-tabaccordion-open-section="2"', else open first element */
        if (this.wrapper.dataset.tabaccordionOpenSection) {
            if (this.wrapper.dataset.tabaccordionOpenSection === "none") {
                return null;
            }

            groupOpen = parseInt(this.wrapper.dataset.tabaccordionOpenSection, 10);

            if (groupOpen >= 1) {
                groupOpen--;
            }
            else {
                groupOpen = 0;
            }
        }
        return groupOpen;
    },

    /**
     * A class name that is set on the `selector.wrapper` depending on the state of the tabaccordion.
     * These class names should be used to style a specific state/layout.
     * They must be defined here without the leading dot.
     * @type {{accordion: String, tabs: String}}
     */
    wrapperTypeClass: {
        accordion: 'accordion',
        tabs: 'tabbed-content'
    },

    /**
     * @type {Object} A behavior type definition as in BEHAVIOR_TYPES
     */
    behavior: BEHAVIOR_TYPES.tabAccordion,

    /**
     * @type {Function} A callback function to be executed, whenever a repaint occurred.
     */
    onAccordionReady: null
};

/**
 * @param {String|Object} behavior
 * @return {Object}
 * @this Accordion
 */
function parseBehavior(behavior) {
    if (typeof behavior === 'string' || behavior instanceof String) {
        if (BEHAVIOR_TYPES[behavior]) {
            return BEHAVIOR_TYPES[behavior];
        }
        else {
            return BEHAVIOR_TYPES.tabAccordion;
        }
    }

    /* Ensure all possible media-query breakpoints do have a behavior definition */
    Object.values(MediaQuerySync.DEVICE_STATE_MAP).forEach(ds => {
        if (!behavior[ds]) {
            behavior[ds] = DISPLAY.accordion;
        }
    });
    return behavior;
}

/**
 * The Accordion object
 */
class Accordion {

    /**
     * @param {Element} wrapper The wrapper element around the tab-accordion (which already must exist).
     * @param {NodeList} sections The single sections/item of an accordion.
     * @param {Object} userConfig An object with all configuration parameters.
     */
    constructor(wrapper, sections, userConfig = {}) {

        if (!(wrapper instanceof Element)) {
            throw new Error('Accordion: The parameter `wrapper` must be of type Element but was of type `' + (typeof wrapper) + '`.');
        }
        if (!(sections instanceof NodeList)) {
            throw new Error('Accordion: The parameter `sections` must be of type NodeList but was of type `' + (typeof sections) + '`.');
        }

        this.wrapper = wrapper;
        this.sections = sections;

        /* Merge default and user configuration */
        this.config = extend(DEFAULT_CONFIG, userConfig);
        /* Make sure behavior is something meaningful */
        this.config.behavior = parseBehavior.call(this, this.config.behavior);

        this.activeSection = 0;
        this.currentDisplay = DISPLAY.none;

        /* Only create the wrapper element once and copy it as necessary (see below) */
        var bodyWrapperTemplate = document.createElement('div');
        bodyWrapperTemplate.className = this.config.sectionBodyWrapperClass;

        /* Get a named reference to this object for usage in event handlers */
        var currentAccordion = this;

        /* Iterate over sections for initial setup */
        this.sections.forEach(function (currentSection) {

            /* Make a copy of the body-wrapper element */
            let currentBodyWrapper = bodyWrapperTemplate.cloneNode(false);

            /* Store references to the header and body-wrapper for easier access, e.g. in setComputedHeights() */
            currentSection[MODULE_PREFIX + "Header"] = currentSection.querySelector(currentAccordion.config.selector.sectionHeading);
            currentSection[MODULE_PREFIX + "Body"] = currentBodyWrapper;

            /* Move existing section-body elements into the new wrapper */
            let bodyElements = currentSection.querySelectorAll(currentAccordion.config.selector.sectionBody);
            for (let k = 0, len = bodyElements.length; k < len; k++) {
                currentBodyWrapper.appendChild(bodyElements[k]);
            }
            currentSection.appendChild(currentBodyWrapper);

            /**
             * Setup click handler on headers
             */
            currentSection[MODULE_PREFIX + "Header"].addEventListener('click', function (event) {
                if (currentAccordion.currentDisplay === DISPLAY.accordion
                    && currentSection.classList.contains(currentAccordion.config.showSectionClass)) {
                    /* Close this section if was open already */
                    currentAccordion.close(currentSection);
                }
                else {
                    /* Or open it, if it was not */
                    currentAccordion.open(currentSection);
                }
            });

            /**
             * Listen for hash changes in the URL that match the section ID.
             * Extract the section ID either from the header element or from the first heading
             * inside the header, that has an ID.
             */
            var currentSectionId;
            if (currentSection[MODULE_PREFIX + "Header"].id) {
                currentSectionId = currentSection[MODULE_PREFIX + "Header"].id;
            } else {
                let idElement = currentSection[MODULE_PREFIX + "Header"].querySelector("h1[id],h2[id],h3[id],h4[id],h5[id],h6[id]");
                if (idElement instanceof Element && idElement.id) {
                    currentSectionId = idElement.id;
                }
            }
            if (currentSectionId) {
                window.addEventListener('hashchange', function (event) {
                    // Only open this item if the has matches the ID
                    if (window.location.hash.replace('#', '') === currentSectionId) {
                        currentAccordion.open(currentSection);
                    }
                });
            }
            /* If the section ID is already the URL hash, open this section on load */
            if (window.location.hash.replace('#', '') === currentSectionId) {
                currentAccordion.config.getInitinalSectionToOpen = function () {
                    return currentSection;
                };
            }

            /* Set a data-attribute to initialized sections, so they can be
             * identified (using the `config.selector.section` CSS selector).
             * This is used to prevent duplicated initialization of sections
             * of nested TabAccordions. */
            currentSection.dataset.tabaccordionSection = "";
        });

        /* Initially set type according to current device state (media query breakpoint) */
        currentAccordion.updateType(MediaQuerySync.getDeviceState());
        /* Change the type as necessary when another breakpoint was reached */
        window.addEventListener(MediaQuerySync.DEVICE_STATE_EVENT_NAME, function (event) {
            currentAccordion.updateType(event.deviceState);
        }, false);

        /* Set computed heights; we listen to the resize event here, since line-breaks may change
         * according to the viewport width before a new media-query breakpoint was reached; */
        currentAccordion.setComputedHeights();
        window.addEventListener('resize', debounce(event => {
            currentAccordion.setComputedHeights();
        }, 50), false);

        /* Open a section on load, if required */
        let initialOpenSection = currentAccordion.config.getInitinalSectionToOpen.call(this);
        if (initialOpenSection !== null) {
            currentAccordion.open(initialOpenSection);
        }
    }

    /**
     * Compute and set heights:
     * - min-height: for the outer wrapper, to ensure following elements stay below the tabbed-content
     * - max-height: for each sections body-wrapper to allow CSS transitions on height for an open-/close-animation
     */
    setComputedHeights() {
        var maxHeight = 0;

        this.sections.forEach(currentSection => {
            let bodyWrapper = currentSection[MODULE_PREFIX + "Body"],
                bodyWrapperHeight = bodyWrapper.scrollHeight;

            /* Store the biggest height of all sections */
            if (bodyWrapperHeight > maxHeight) {
                maxHeight = bodyWrapperHeight;
            }

            /* Set new max-height */
            bodyWrapper.style.maxHeight = bodyWrapperHeight + 'px';
        });

        /* Get example heading height to add it to the overall height */
        var headingHeight = this.sections[0][MODULE_PREFIX + "Header"].scrollHeight;
        this.wrapper.style.minHeight = (maxHeight + headingHeight) + 'px';
    }

    /**
     * @param {String} deviceState One of the MediaQuerySync.DEVICE_STATE_MAP values
     */
    updateType(deviceState) {
        this.currentDisplay = this.config.behavior[deviceState];

        /* switch to type */
        switch (this.config.behavior[deviceState]) {
            case DISPLAY.none:
                this.removeTabAccordion();
                break;
            case DISPLAY.tabs:
                this.makeTabs();
                break;
            case DISPLAY.accordion:
            default:
                this.makeAccordion();
        }
    }

    /**
     * Open a specific section
     *
     * @param {Element|Number} section
     */
    open(section) {

        /* get a single section from numeric index */
        if (!(section instanceof Element)) {
            section = this.sections.item(parseInt(section, 10));
        }

        /* close the last section */
        this.close(this.activeSection);

        /* open section */
        section.classList.add(this.config.showSectionClass);

        /* set this section as current */
        this.activeSection = section;
    }

    /**
     * Close one or all sections
     *
     * @param {Element|Number|undefined} section
     */
    close(section) {
        /* close all sections without parameter */
        if (typeof section === 'undefined') {
            this.sections.forEach(currentSection => this.close(currentSection));
            return;
        }

        /* get a single section from numeric index */
        if (!(section instanceof Element)) {
            section = this.sections.item(parseInt(section, 10));
        }

        /* close section */
        section.classList.remove(this.config.showSectionClass);
    }

    /**
     * Execute callback function onAccordionReady
     */
    accordionReadyCallback() {
        if (this.config.onAccordionReady !== null) {
            var that = this;
            setTimeout(function () {
                that.config.onAccordionReady(that);
            }, 100);
        }
    }

    /**
     * Remove any classes, switch back to normal panels
     */
    removeTabAccordion() {
        this.wrapper.classList.remove(this.config.wrapperTypeClass.tabs);
        this.wrapper.classList.remove(this.config.wrapperTypeClass.accordion);
    }

    /**
     * Turn the current section group into an accordion
     */
    makeAccordion() {
        this.wrapper.classList.remove(this.config.wrapperTypeClass.tabs);
        this.wrapper.classList.add(this.config.wrapperTypeClass.accordion);
        this.accordionReadyCallback();
    }

    /**
     * Turn the current section group into tabbed content
     */
    makeTabs() {
        this.wrapper.classList.add(this.config.wrapperTypeClass.tabs);
        this.wrapper.classList.remove(this.config.wrapperTypeClass.accordion);
        this.accordionReadyCallback();
        this.open(0);
    }
}

/**
 * Helper function to set up multiple TabAccordion in the current document
 * according to a user configuration.
 *
 * @param {Object} userConfig Custom overwrites for the class names as in the DEFAULT_CONFIG object.
 * @returns {Array} A list of Accordion objects
 */
function setup(userConfig = {}) {

    /* Merge default and user configuration */
    var config = extend(DEFAULT_CONFIG, userConfig);

    var accordions = [],
        accordionGroups = document.querySelectorAll(config.selector.wrapper),
        currentAccordion;

    for (let i = accordionGroups.length; i--;) {
        try {
            let sections = accordionGroups[i].querySelectorAll(config.selector.section);

            if (accordionGroups[i].dataset.tabaccordionBehaviorType) {
                config.behavior = accordionGroups[i].dataset.tabaccordionBehaviorType;
            }

            if (sections.length > 0) {
                currentAccordion = new Accordion(
                    accordionGroups[i],
                    sections,
                    config
                );

                /* Store a reference to return */
                accordions.push(currentAccordion);
            }
        }
        catch (error) {
            console.error(error.stack);
        }
    }
    /* Return the list of accordion elements for further manipulation */
    return accordions;
}

export {
    DISPLAY,
    BEHAVIOR_TYPES,
    // Expose the Accordion object to the parent scope.
    // This can be used to manually create a new accordion instance.
    Accordion,
    setup
};
