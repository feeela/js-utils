/**
 * Display the amount of bytes (formatted) the localStorage or sessionStorage
 * for the current domain stores.
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 *
 * @todo Add support for window.indexedDB
 */

/**
 * @link    https://stackoverflow.com/a/20732091/341201
 * @author  Andrew V. (https://stackoverflow.com/users/3127587/andrew-v)
 * @license CC BY-SA 3.0
 */
function toHumanFileSize(size) {
    var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

/**
 * @param {Storage} storage
 * @return {Array}
 */
function getStorageSize(storage = localStorage) {
    return Object.keys(storage)
        .map((storageKey) => {
            return {
                name: storageKey,
                size: (storage[storageKey].length * 2)
            };
        })
        .sort((a, b) => b.size - a.size)
        .map(entry => {
            entry.size = toHumanFileSize(entry.size);
            return entry;
        });
}

function getLocalStorageSize() {
    return getStorageSize(localStorage);
}

function getSessionStorageSize() {
    return getStorageSize(sessionStorage);
}

/**
 * Returns approximate size of all caches (in bytes)
 *
 * To test this function see for example: https://googlechrome.github.io/samples/service-worker/window-caches/
 *
 * This function is based on:
 * @link https://stackoverflow.com/a/43372970/341201
 * @author mjs (https://stackoverflow.com/users/11543/mjs)
 * @license CC BY-SA 3.0
 *
 * @param {CacheStorage} cacheStorage
 * @return {Promise}
 */
async function getCacheSize(cacheStorage = caches) {
    return cacheStorage.keys().then(storageKeys => {
        return Promise.all(
            storageKeys.map(storageKey => {
                return cacheStorage.open(storageKey).then(cache => {
                    return cache.keys().then(requestArray => {
                        return Promise.all(
                            requestArray.map(request => {
                                return cache.match(request).then(response => {
                                    return response.clone().blob().then(blob => {
                                        return {
                                            cache: storageKey,
                                            object: response.url,
                                            size: blob.size
                                        }
                                    })
                                })
                            }) // EO requestArray.map()
                        );
                    });
                })
            }) // EO storageKeys.map()
        ).then(cacheSizes => {
            let output = [];
            cacheSizes.forEach(cacheSizesArray => cacheSizesArray.forEach(cacheSizesLine => output.push(cacheSizesLine)));
            return output.sort((a, b) => b.size - a.size)
                .map(entry => {
                    entry.size = toHumanFileSize(entry.size);
                    return entry;
                });
        });
    });
}

function showAllStorageSizes() {
    console.log("localStorage");
    console.table(getLocalStorageSize());

    console.log("sessionStorage");
    console.table(getSessionStorageSize());

    console.log("caches");
    (async function () {
        let cacheSizes = await getCacheSize();
        console.table(cacheSizes);
    })();
}
showAllStorageSizes();

export {
    showAllStorageSizes as default,
    getLocalStorageSize,
    getSessionStorageSize,
    getCacheSize,
    toHumanFileSize
};
