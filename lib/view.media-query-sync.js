/**
 * MediaQuerySync – a module to reuse CSS media queries in JavaScript
 *
 * This script relies on a CSS snippet, that sets the z-index on a dummy element
 * for each media query breakpoint. See:
 * @link ./view.media-query-sync.css
 *
 * Usage:
     import * as MediaQuerySync from "./view.media-query-sync.js";

     console.log(MediaQuerySync.getDeviceState());

     window.addEventListener(MediaQuerySync.DEVICE_STATE_EVENT_NAME, ev => {
       console.log(ev.deviceState);
    });
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 *
 * based on:
 * @link http://davidwalsh.name/device-state-detection-css-media-queries-javascript
 */

import {debounce} from "./helper.js";

let deviceState;

let lastDeviceState;

/**
 * This map defines the z-index values used for the respective breakpoints
 * @type {Object}
 */
const DEVICE_STATE_MAP = {
    1: 'xxs', // @media all
    2: 'xs',
    3: 'sm',
    4: 'md',
    5: 'lg',
    6: 'xl'
};

const DEVICE_STATE_EVENT_NAME = "deviceStateChanged";
/**
 * Use window.addEventListener("deviceStateChanged", callback) to react on breakpoint changes
 * @type {CustomEvent}
 */
const deviceStateEvent = new CustomEvent(DEVICE_STATE_EVENT_NAME, {deviceState: DEVICE_STATE_MAP[1]});

// Create the element, on which we apply the style with different media-queries
let deviceStateDetectionElement = document.createElement('div');
deviceStateDetectionElement.id = 'device-state-detection';
document.body.appendChild(deviceStateDetectionElement);

if (!window.getComputedStyle) {
    window.getComputedStyle = function (el, pseudo) {
        this.el = el;
        this.getPropertyValue = function (prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        };
        return this;
    }
}

function syncMediaQuery() {
    var deviceStateIndex = parseInt(window.getComputedStyle(deviceStateDetectionElement).getPropertyValue('z-index'), 10);
    if (typeof DEVICE_STATE_MAP[deviceStateIndex] === 'undefined') {
        deviceState = DEVICE_STATE_MAP[1];
    }
    else {
        deviceState = DEVICE_STATE_MAP[deviceStateIndex];
    }

    if (deviceState !== lastDeviceState) {
        lastDeviceState = deviceState;

        if (typeof deviceStateEvent !== 'undefined') {
            // trigger global event
            deviceStateEvent.deviceState = deviceState;
            window.dispatchEvent(deviceStateEvent);
        }
    }
}

// Setup an event listener for resize and fire the DEVICE_STATE_EVENT_NAME event
// if another design breakpoint was reached.
window.addEventListener('resize', debounce(syncMediaQuery), false);

// Initial call to provide a basic state for other scripts and fire the CustomEvent
// on startup
syncMediaQuery();

/**
 * Return the current device state
 *
 * @returns {String} A named design breakpoint (one of the DEVICE_STATE_MAP values)
 */
function getDeviceState() {
    if (!deviceState) {
        syncMediaQuery();
    }
    return deviceState;
}

export {
    getDeviceState,
    DEVICE_STATE_MAP,
    DEVICE_STATE_EVENT_NAME
};
