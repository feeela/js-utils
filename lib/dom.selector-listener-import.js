/**
 * selectorListenerImport is a conditional import
 * to load modules based on a CSS selector.
 *
 * This enables modules to be loaded automagically when a matching element
 * is inserted to the page, e.g. after an AJAX operation.
 *
 * The module exports the function `selectorListenerImport(selector, deps, callback)`.
 * The callback retrieves at least two properties: `selector` (arg[0], the input selector as refrence)
 * and `element` (arg[1], the found element), followed by the dependencies
 * in the order of the input array. The callback is executed each time
 * an element that matches the selector was found in the page.
 *
 * Example:
 import selectorListenerImport from "./selector-listener-import.js";

 selectorListenerImport(
 ".some-element",
 ['./depA.js', './depB.js'],
 (selector, element, depA, depB) => {
            console.log(selector, element, depA, depB);
        });
 *
 *
 * @author    Thomas Heuer <technik@thomas-heuer.eu>
 * @copyright Copyright © 2021 Thomas Heuer
 * @licence   MIT License
 */
import {importStyles} from "./dom.js";

/**
 * Create the animation styles for the given CSS selector and return the animation ID.
 *
 * @private
 * @param {String} selector
 * @return {string}
 */
function createAnimationEventStyles(selector) {
    let animationID = "cdl-" + (Math.floor(Math.random() * (99999 - 10000 + 1)) + 1000);
    importStyles(selector, "animation-duration: 1ms !important; animation-name: " +
        animationID + " !important;");
    importStyles("@keyframes " + animationID, "from {opacity:.99} to {opacity:1}");
    return animationID;
}

/**
 * @type {{animationID: {selector: String, imports: {deps: Array, callback: Function} }}}
 */
let animations = {};

/**
 * @type {{selector: String}}
 */
let animationIDs = {};

/**
 * Listen to CSS animation-start events
 */
document.addEventListener("animationstart", function (event) {
    if (event.animationName in animations) {
        // This is an internal event - stop it immediately
        event.stopImmediatePropagation();

        // Iterate over possibly multiple callbacks for the selector matched here
        animations[event.animationName].imports.map(scriptImport => {

            // Turn string list of dependencies into an array of Promises
            let dependencyPromises = scriptImport.deps.map(dep => {
                let depProm;
                depProm = import(dep).catch(importError => console.error(importError));
                return depProm;
            });

            // Wait until all modules are loaded (all Promises are resolved) and finally execute the callback
            Promise.all(dependencyPromises)
                .then(modules => {
                    scriptImport.callback.apply(null, [
                        animations[event.animationName].selector,
                        event.target,
                        ...modules
                    ]);
                });
        });
    }
}, true);

/**
 * Setup a callback with dependencies and listen to the given CSS selector.
 *
 * @param {String} selector The CSS selector to listen to
 * @param {Array} deps Dependencies as array of string
 * @param {Function} callback This function uses a callback instead of being based on a Promise, to allow multiple executions
 */
function selectorListenerImport(selector, deps, callback) {
    // Re-use existing animation for the given selector or create the animation styles
    let animationID;
    if (selector in animationIDs) {
        animationID = animationIDs[selector];
    } else {
        animationID = createAnimationEventStyles(selector);
        animationIDs[selector] = animationID;
    }

    // Store the deps & callback using the animationID as key
    if (!animations[animationID]) {
        animations[animationID] = {
            "selector": selector,
            "imports": []
        }
    }

    animations[animationID]["imports"].push({
        "deps": deps,
        "callback": callback
    });
}

export {selectorListenerImport as default};
