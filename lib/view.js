/**
 * UI helper
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 */


/**
 * Deferred iframes: include iframes only after our own content was already loaded.
 * CSS class names will get copied to the new iframe element.
 *
 * Example Markup:
 *     <div data-iframe="//www.youtube.com/embed/RsASi1EBDys" class="video"></div>
 *
 * @param {String} placeholderSelector The selector to find iframe placeholder tags
 */
function deferredIframes(placeholderSelector) {
    var iframes = document.querySelectorAll(placeholderSelector);
    for (let i = iframes.length; i--;) {
        let iframe = document.createElement('iframe');
        iframe.src = iframes[i].dataset.iframe;
        iframe.className = iframes[i].className;
        iframes[i].parentNode.replaceChild(iframe, iframes[i]);
    }
}

var loadIndicatorIcon;

/**
 * Show or hide the big AJAX load icon in the middle of the screen.
 *
 * @param {Boolean|String} status Set to TRUE to show the loader and to FALSE to hide it.
 */
function loadIndicator(status) {
    if (!(loadIndicatorIcon instanceof Element)) {
        /* Create a new ajax loader element */
        loadIndicatorIcon = document.createElement('div');
        loadIndicatorIcon.className = 'load-spinner icon icon-loader icon-replace icon-spin';
        document.documentElement.appendChild(loadIndicatorIcon);
    }

    if (status === true) {
        loadIndicatorIcon.style.display = 'block';
    }
    else {
        loadIndicatorIcon.style.display = 'none';
    }
}

/**
 * Display a load indicator on a given element (e.g. a button)
 *
 * @param {Element} parent The element which should get a load indicator.
 * @param {Boolean} status Set to TRUE to show the loader and to FALSE to hide it.
 */
function inlineLoadIndicator(parent, status) {
    var inlineLoadIndicatorIcon = parent.querySelector('.icon-loader.icon-spin');

    if (!(inlineLoadIndicatorIcon instanceof Element)) {
        /* Create a new ajax loader element */
        inlineLoadIndicatorIcon = document.createElement('span');
        inlineLoadIndicatorIcon.className = 'icon icon-loader icon-spin';
        parent.appendChild(inlineLoadIndicatorIcon);
    }

    if (status === true) {
        parent.classList.add('has-load-icon');
        inlineLoadIndicatorIcon.style.display = 'block';
    }
    else {
        inlineLoadIndicatorIcon.style.display = 'none';
        parent.classList.remove('has-load-icon');
    }
}

/**
 * Add a button to an element that fires `window.print()`on click.
 *
 * @param {String|Element} target The button will get inserted after the given element. Pass in a selector or an actual element object.
 * @param {String} label The button label
 * @param {String} cssClass The button classes
 */
function addPrintButton(target, label, cssClass) {

    if (!target) {
        return;
    }

    if (!( target instanceof Element )) {
        target = document.querySelector(target);
        if (target === null) {
            return;
        }
    }

    var button = document.createElement('button');
    button.innerHTML = label;
    button.className = cssClass;

    button.onclick = function (event) {
        event.preventDefault();
        window.print();
    };

    target.parentNode.insertBefore(button, target.nextSibling);
}

export {
    loadIndicator,
    inlineLoadIndicator,
    deferredIframes,
    addPrintButton
};
