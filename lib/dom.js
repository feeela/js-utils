/**
 * Generic functions to manipulate the DOM
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 */


/**
 * Traverse the DOM in different directions
 * and return a list of all parents, children or siblings (depending on "direction").
 *
 * This function was based upon jQuery's "closest" function
 * @see https://github.com/jquery/jquery/blob/master/src/traversing.js
 *
 * @param {Element} element Where to start
 * @param {String} direction One of: parentNode, firstChild, lastChild, nextSibling, previousSibling
 * @param {String} className If a class name was passed, than a single element will get returned if it has that class name and is a parent, child or sibling of the given element.
 * @returns {Element|[Element]}
 */
function traverse(element, direction, className) {
    var matched = [],
        direction = direction || 'parentNode';

    while ((element = element[direction]) && element.nodeType === 1) {
        if (className && element.classList.contains(className)) {
            return element;
        }
        else {
            matched.push(element);
        }
    }

    return matched;
}

/**
 * Turn a string in to an single HTML element or a NodeList.
 *
 * @param {String} html
 * @param {Boolean} returnNodeList
 * @returns {Element|NodeList}
 */
function stringToHtml(html, returnNodeList = false) {
    var wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    return returnNodeList ? wrapper.childNodes : wrapper.firstChild;
}

/**
 * @type {CSSStyleSheet} A separate stylesheet for use with JS
 */
let stylesheet;

/**
 * Import stylesheets into the current document. Call once for each block.
 *
 * Example:
 * <code>
    importStyles(".my-whatever", "color: #333333; background: #f4f4f4;");
 * </code>
 *
 * @param {String} selector
 * @param {String} cssText
 */
function importStyles(selector, cssText) {
    // Create a separate style element for use with JavaScript
    if (!stylesheet) {
        let styleNode = document.getElementsByTagName("head")[0].appendChild(document.createElement("style"));
        stylesheet = styleNode.sheet || styleNode.styleSheet;
    }

    if (typeof selector !== "string" || typeof cssText !== "string") {
        throw new Error("importStyles wrong arguments", arguments);
    }

    // insert rules one by one because of several reasons:
    // 1. IE8 does not support comma in a selector string
    // 2. if one selector fails it doesn't break others
    selector.split(",").forEach(function (selector) {
        try {
            if (stylesheet.cssRules) {
                stylesheet.insertRule(selector + "{" + cssText + "}", stylesheet.cssRules.length);
            } else if (selector[0] !== "@") {
                stylesheet.addRule(selector, cssText);
            } else {
                // addRule doesn't support at-rules, use cssText instead
                stylesheet.cssText += selector + "{" + cssText + "}";
            }
        } catch (err) {
            console.error(err);
            // ignore invalid rules and move on
        }
    });
}

export {traverse, stringToHtml, importStyles};
