/**
 * Network- and URL-related functions
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 */
import {extend, hash} from './helper.js';

/**
 * Serialize and encode an object as HTTP URL parameter.
 * This function does not prepend the question mark
 *
 * @param {Object} data
 * @returns {String} An string of URL encoded of parameters
 */
function encodeUriData(data) {
    return Object.keys(data).map(function(key) {
        return [key, data[key]].map(encodeURIComponent).join('=');
    }).join('&');
}

/**
 * add GET parameter to URL query string
 *
 * @param url
 * @param key
 * @param value
 * @returns {string} modified url with new GET parameter
 */
function addGetParameterToUrl(url, key, value) {

    if (key) {
        var newParam;
        key = encodeURI(key);

        var urlParams = url.split('?');

        if (value) {
            value = encodeURI(value);
            newParam = key + '=' + value;
        } else {
            newParam = key;
        }

        url += (url.split('?')[1] ? '&' : '?') + newParam;
    }

    return url;
}

/**
 * An XHR implementation that returns a Promise.
 *
 * Options:
 * <code>
 *  {
 *    method: 'GET',
 *    async: true,
 *
 *    // The expected return data-type
 *    type: 'json',
 *
 *    // The post data to send. The following types are allowed:
 *    // ArrayBufferView, Blob, Document, DOMString, FormData, URLSearchParams
 *    data: '',
 *
 *    // A list of additional HTTP request header to be set.
 *    header: {},
 *
 *    // Possible event handlers for XMLHttpRequest.
 *    // By default the load, error & abort event handlers are used to resolve or reject the promise.
 *    // Each callback receives the promise handling functions as arguments.
 *    // `this` is set to the XHR object inside each callback.
 *    callbacks: {
 *
 *      // Progress has begun. Executed Once, at first.
 *      loadstart: function( resolve, reject ) {  },
 *
 *      // In progress. Executed Zero or more, after loadstart has been dispatched.
 *      progress: function( resolve, reject ) {  },
 *
 *      // Progression is successful. Executed Zero or once.
 *      load: function( resolve, reject ) {  },
 *
 *      // Progression failed. Executed Zero or once, after the last progress has been dispatched, or after loadstart has been dispatched if progress has not been dispatched.
 *      error: function( resolve, reject ) {  },
 *
 *      // Progression is terminated. Executed Zero or once.
 *      abort: function( resolve, reject ) {  },
 *
 *      // Progress has stopped. Executed Once, after one of error, abort, or load has been dispatched.
 *      loadend: function( resolve, reject ) {  }
 *    },
 *
 *    // The same events can also be used to monitor the upload status.
 *    uploadCallbacks: {}
 *  }
 *  </code>
 *
 *  Examples:
 *  <code>
 xhr( "/my/api/shopping-cart" )
 .then( cartdata => {
            // display a mini cart dropdown using cartdata JSON
        } );

 xhr( "someTemplate.mustache", {type: "text"} )
 .then( template => {
            // use plain text template here
        } );
 *  </code>
 *
 * @param {String} url
 * @param {Object} userOptions
 * @returns {Promise}
 */
function xhr(url, userOptions) {

    return new Promise(function(resolve, reject) {

        var options = {
            method: 'GET',
            async: true,
            type: 'json',
            data: '',
            header: {},
            callbacks: {
                load: function() {
                    if (this.status !== 200) {
                        reject(new Error('HTTP ' + this.status + ' ' + this.statusText));
                        return;
                    }

                    if (!this.response) {
                        reject(new Error('Response is empty.'));
                    } else {
                        resolve(this.response);
                    }
                },
                error: function() {
                    reject(new Error('Request failed.'));
                },
                abort: function() {
                    reject(new Error('Request stopped.'));
                },
            },
            uploadCallbacks: {},
        };

        options = extend(options, userOptions);

        var request = new XMLHttpRequest();
        request.open(options.method, url, options.async);

        request.responseType = options.type;

        /* Set request header */
        for (let header in options.header) {
            request.setRequestHeader(header, options.header[header]);
        }

        /* Add callbacks */
        for (let eventType in options.callbacks) {
            if (options.callbacks.hasOwnProperty(eventType)) {
                /* Using IIFE with anonymous function to scope eventHandler for each event listener */
                request.addEventListener(eventType, (function() {
                    var eventHandler = options.callbacks[eventType];
                    return function(event) {
                        eventHandler.call(event.target, resolve, reject);
                    };
                })(), false);
            }
        }

        for (let uploadEventType in options.uploadCallbacks) {
            if (options.uploadCallbacks.hasOwnProperty(uploadEventType)) {
                /* Using IIFE with anonymous function to scope eventHandler for each event listener */
                request.upload.addEventListener(uploadEventType, (function() {
                    var eventHandler = options.uploadCallbacks[uploadEventType];
                    return function(event) {
                        eventHandler.call(event.target, resolve, reject);
                    };
                })(), false);
            }
        }

        request.send(options.data);

    });
}

/**
 * This function includes an external script, which can be used to load JSONP data.
 *
 * This function creates one global callback for each callback.
 *
 * Per default a parameter 'callback' is added to the URL to identify the global callback.
 * If the server need another name for that parameter it can be specified through the
 * argument `callbackParameterName`.
 *
 * @param {String} url
 * @param {Function} callback
 * @param {String} callbackParameterName
 */
function jsonp(url, callback, callbackParameterName) {
    callbackParameterName || (callbackParameterName = 'callback');

    var suggestLoadScriptElement,
        callbackParameter = {};

    callbackParameter[callbackParameterName] = 'jsonpCallback' + hash(callback);

    suggestLoadScriptElement = document.createElement('script');
    suggestLoadScriptElement.src = url + '&' + encodeUriData(callbackParameter);
    suggestLoadScriptElement.id = callbackParameter[callbackParameterName];
    document.head.appendChild(suggestLoadScriptElement);

    /* set a global reference to the JSONP callback */
    window[callbackParameter[callbackParameterName]] = (function() {
        /* store a local reference to the current script element ID */
        var scriptElementId = callbackParameter[callbackParameterName];
        return function(jsonpData) {
            callback(jsonpData);
            var scriptElement = document.head.querySelector('#' + scriptElementId);
            if (scriptElement instanceof Element) {
                document.head.removeChild(suggestLoadScriptElement);
            }
        };
    })();
}

var cookies = {};

/**
 * Read a single cookie identified by its name.
 *
 * @param {String} key The cookie name
 * @returns {String}
 */
function readCookie(key) {
    if (!cookies[key]) {
        let result;
        cookies[key] = (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
    }
    return cookies[key];
}

export {
    encodeUriData,
    addGetParameterToUrl,
    xhr,
    jsonp,
    readCookie,
};
