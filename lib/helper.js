/**
 * This is common module for often used functions.
 *
 * The functions must be without further dependencies outside this module scope.
 *
 * @author  Thomas Heuer <projekte@thomas-heuer.eu>
 * @license http://opensource.org/licenses/MIT – The MIT License (MIT)
 */

/**
 * Returns a function, that, as long as it continues to be invoked, will not be triggered.
 * The function will be called after it stops being called for `wait` milliseconds.
 * If `immediate` is passed, trigger the function on the leading edge, instead of the trailing.
 *
 * This function was taken from Underscore.js as of 2014
 * @link https://davidwalsh.name/javascript-debounce-function
 *
 * @param {Function} func
 * @param {Number} wait
 * @param {Boolean} immediate
 * @returns {Function}
 */
function debounce(func, wait = 250, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

/**
 * Recursively merge properties of two objects
 *
 * @param {Object} defaults Default settings
 * @param {Object} options User options
 * @returns {Object} Merged values of defaults and options
 */
function extend(defaults, options) {

    function copy(obj1, obj2, prop) {
        try {
            if (obj2[prop].constructor === Object) {
                obj1[prop] = extend(obj1[prop] || {}, obj2[prop]);
            }
            else {
                obj1[prop] = obj2[prop];
            }
        }
        catch (error) {
            obj1[prop] = obj2[prop];
        }
        return obj1;
    }

    var extended = {};
    var prop;
    for (prop in defaults) {
        if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
            copy(extended, defaults, prop);
        }
    }
    for (prop in options) {
        if (Object.prototype.hasOwnProperty.call(options, prop)) {
            copy(extended, options, prop);
        }
    }
    return extended;
}

/**
 * Set an event-listener to a parent element and use it to capture events on one of it's children.
 *
 * This function works like the long form of jQuery's "on" function.
 *
 * @param {String|Element} elementSelector
 * @param {String} eventName
 * @param {String|NodeList} selector
 * @param {Function} fn
 */
function on(elementSelector, eventName, selector, fn) {

    var element,
        possibleTargets;

    if (elementSelector instanceof Element) {
        element = elementSelector;
    }
    else {
        element = document.querySelector(elementSelector);
    }

    if (selector instanceof NodeList) {
        possibleTargets = selector;
    }
    else {
        possibleTargets = element.querySelectorAll(selector);
    }

    element.addEventListener(eventName, function (event) {
        var target = event.target;

        for (var i = 0, l = possibleTargets.length; i < l; i++) {
            var el = target;
            var p = possibleTargets[i];

            while (el && el !== element) {
                if (el === p) {
                    return fn.call(p, event);
                }

                el = el.parentNode;
            }
        }
    });
}

/**
 * Get a simple integer hash from some string input.
 *
 * This is a JavaScript implementation of Java’s String.hashCode() method.
 *
 * @param {String|*} input
 * @returns {Number}
 */
function hash(input) {
    input = input + '';

    var hash = 0,
        char,
        inputLength = input.length;

    if (inputLength == 0) {
        return hash;
    }

    for (var i = 0; i < inputLength; i++) {
        char = input.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }

    if (hash < 0) {
        hash = hash * -1;
    }

    return hash;
}

/**
 * Implementation of toFixed() that treats floats more like decimals in accounting.
 *
 * Fixes binary rounding issues (eg. (0.615).toFixed(2) === "0.61") that present
 * problems for accounting- and finance-related software.
 *
 * @param {Number} value
 * @param {Number} precision
 * @returns {String}
 */
function toFixed(value, precision) {
    var power = Math.pow(10, precision);
    /* Multiply up by precision, round accurately, then divide and use native toFixed() */
    return (Math.round(value * power) / power).toFixed(precision);
}

/**
 * Format a number, with comma-separated thousands and custom precision/decimal places
 * Localise by overriding the precision and thousand/decimal separators
 *
 * This function was extracted from the accounting.js library
 * @see https://github.com/openexchangerates/accounting.js
 *
 * @param {Number|Array} number
 * @param {Number} precision
 * @param {String} thousand
 * @param {String} decimal
 * @returns {String}
 */
function formatNumber(number, precision, thousand, decimal) {

    /* Resursively format arrays */
    if (Array.isArray(number)) {
        return number.map(function (val) {
            return formatNumber(val, precision, thousand, decimal);
        });
    }

    precision || (precision = 2);
    thousand || (thousand = '.');
    decimal || (decimal = ',');

    /* Check and normalise the value of precision (must be positive integer) */
    precision = Math.round(Math.abs(precision));

    /* Do some calc */
    var negative = number < 0 ? '-' : '',
        base = parseInt(toFixed(Math.abs(number || 0), precision), 10) + '',
        mod = base.length > 3 ? base.length % 3 : 0;

    /* Format the number */
    return negative
        + (mod ? base.substr(0, mod) + thousand : '')
        + base.substr(mod).replace(/(\d{3})(?=\d)/g, '$1' + thousand)
        + (precision ? decimal + toFixed(Math.abs(number), precision).split('.')[1] : '');
}

/**
 * Replaces numeric placeholders in strings with the function arguments
 *
 * formatString(format, arg{0}, arg{1}, …, arg{n})
 * formatString("Input {1} with {0}", "replacements", "string")
 *
 * @param {String} format
 * @param {*} replacements 0…n
 * @returns {String}
 */
function formatString(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
}

export {
    debounce,
    extend,
    toFixed,
    formatNumber,
    formatString,
    hash,
    on
}
