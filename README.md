# JS Utils by Thomas Heuer

**Author**: Thomas Heuer <projekte@thomas-heuer.eu>  
**License**: [The MIT License (MIT)](LICENSE)

## Libraries and utility functions

[My library without a name](lib/) – This is a set of often used utility functions
and modules that may be used via ES6 module imports or be copied individually into your code.


### [MediaQuerySync](lib/view.media-query-sync.js)

A module to reuse [CSS media queries](lib/view.media-query-sync.css) in JavaScript.

### [TabAccordion](playground/tabaccordion/README.md)

An accordion, that may change its display to tabbed content based on the screen width.

### [Selector Listener Import](playground/selector-listener-import/README.md)

Conditional, dynamic dependency loading of JS modules based on a CSS selector.


### [helper.js](lib/helper.js) – Often used functions without further dependencies


#### debounce(func, wait, immediate) ⇒ `function`
Returns a function, that, as long as it continues to be invoked, will not be triggered.
The function will be called after it stops being called for `wait` milliseconds.
If `immediate` is passed, trigger the function on the leading edge, instead of the trailing.

This function was taken from [Underscore.js as of 2014](https://davidwalsh.name/javascript-debounce-function) 

| Param | Type | Default |
| --- | --- | --- |
| func | `function` |  | 
| wait | `Number` | `250` | 
| immediate | `Boolean` |  | 

#### extend(defaults, options) ⇒ `Object`
Recursively merge properties of two objects

**Returns**: `Object` - Merged values of defaults and options  

| Param | Type | Description |
| --- | --- | --- |
| defaults | `Object` | Default settings |
| options | `Object` | User options |

#### on(elementSelector, eventName, selector, fn)
Set an event-listener to a parent element and use it to capture events on one of it's children.

This function works like the long form of jQuery's `on` function.


| Param | Type |
| --- | --- |
| elementSelector | `String` \| `Element` | 
| eventName | `String` | 
| selector | `String` \| `NodeList` | 
| fn | `function` | 

#### hash(input) ⇒ `Number`
Get a simple integer hash from some string input.

This is a JavaScript implementation of Java’s `String.hashCode()` method.


| Param | Type |
| --- | --- |
| input | `String` \| `\*` | 

#### toFixed(value, precision) ⇒ `String`
Implementation of `toFixed()` that treats floats more like decimals in accounting.

Fixes binary rounding issues (eg. `(0.615).toFixed(2) === "0.61"`) that present
problems for accounting- and finance-related software.


| Param | Type |
| --- | --- |
| value | `Number` | 
| precision | `Number` | 

#### formatNumber(number, precision, thousand, decimal) ⇒ `String`
Format a number, with comma-separated thousands and custom precision/decimal places
Localise by overriding the precision and thousand/decimal separators

This function was extracted from the [accounting.js](https://github.com/openexchangerates/accounting.js) library

| Param | Type | Default |
| --- | --- | --- |
| number | `Number` \| `Array` | | 
| precision | `Number` | `2` |
| thousand | `String` | `.` |
| decimal | `String` | `,` |

#### formatString(format, replacements) ⇒ `String`
Replaces numeric placeholders in strings with the function arguments

```javascript
formatString(format, arg{0}, arg{1}, …, arg{n})
formatString("Input {1} with {0}", "replacements", "string")
```

| Param | Type | Description |
| --- | --- | --- |
| format | `String` |  |
| replacements | `String` | 0…n arguments as replacements |


### [environment.js](lib/environment.js) – Utility functions to get information from the client about itself

#### isMobileBrowser() ⇒ <code>Boolean</code>
Tests the navigator string to a regexp to determine if this is a mobile browser.

#### isTouchDevice() ⇒ <code>Boolean</code>
Detect if the current device is a touch device using temporary elements and CSS media queries.
Inspired by Modernizr but trimmed to essential code.

#### getMobileOperatingSystem() ⇒ <code>String</code>
Determine the mobile operating system using a regexp on the navigator string.

**Returns**: <code>String</code> - One of `iOS`, `Android`, `Windows` or `unknown`  
#### isUserAgent(uaName, returnData) ⇒ <code>Boolean</code> \| <code>Array</code>
Ask for a specific user agent, e.g. "Internet Explorer".

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| uaName | <code>String</code> |  | One of `Opera Mini`, `Opera Mobile`, `Opera`, `Android`, `BlackBerry`, `Chrome iOS`, `Safari iOS`, `Chrome`, `Internet Explorer`, `Firefox`, `Safari` |
| returnData | <code>Boolean</code> | <code>false</code> | Set to true to get the user agent data instead of a Boolean value |

#### getIOSVersion() ⇒ <code>Array</code> \| <code>Boolean</code>
Tries to determine the current iOS version. Returns `false` for non-iOS devices.

**Returns**: <code>Array</code> \| <code>Boolean</code> - A split of the three parts of the iOS version number, i. e 7.1.2 => [7, 1, 2].  


### [net.js](lib/net.js) – Network- and URL-related functions


#### encodeUriData(data) ⇒ `String`
Serialize and encode an object as HTTP URL parameter.
This function does not prepend the question mark

**Returns**: `String` - An string of URL encoded of parameters  

#### addGetParameterToUrl(url, key, value) ⇒ `string`
add GET parameter to URL query string

**Returns**: `string` - modified url with new GET parameter  

#### xhr(url, userOptions) ⇒ `Promise`
An XHR implementation that returns a Promise.

 Examples:
```javascript
xhr( "/my/api/shopping-cart" )
    .then( cartdata => {
        // display a mini cart dropdown using cartdata JSON
    } );

xhr( "someTemplate.mustache", {type: "text"} )
    .then( template => {
        // use plain text template here
    } );
```

#### jsonp(url, callback, callbackParameterName)
This function includes an external script, which can be used to load JSONP data.

This function creates one global callback for each callback.

Per default a parameter 'callback' is added to the URL to identify the global callback.
If the server requires a different name for that parameter, it can be specified through the
argument `callbackParameterName`.

#### readCookie(key) ⇒ `String`
Read a single cookie identified by its name.

### [dom.js](lib/dom.js) – Functions to manipulate the DOM

#### traverse(element, direction, className) ⇒ `Element` \| `[Element]`
Traverse the DOM in different directions
and return a list of all parents, children or siblings (depending on "direction").

This function was based upon [jQuery's "closest" function](https://github.com/jquery/jquery/blob/master/src/traversing.js).

| Param | Type | Description |
| --- | --- | --- |
| element | `Element` | Where to start |
| direction | `String` | One of: `parentNode`, `firstChild`, `lastChild`, `nextSibling`, `previousSibling` |
| className | `String` | If a class name was passed, than a single element will get returned if it has that class name and is a parent, child or sibling of the given element. |

#### stringToHtml(html, returnNodeList) ⇒ `Element` \| `NodeList`
Turn a string in to an single HTML element or a NodeList.


| Param | Type | Default |
| --- | --- | --- |
| html | `String` | 
| returnNodeList | `Boolean` | false |

#### importStyles(selector, cssText)
Import stylesheets into the current document. Call once for each block.

Example:
```javascript
importStyles(".my-whatever", "color: #333333; background: #f4f4f4;");

/* @-rules are also possible */
importStyles(".animate-this", "animation-duration: 500ms; animation-name: moveit;");
importStyles("@keyframes moveit", "from {left: 0} to {left: 100px}");
```

| Param | Type |
| --- | --- |
| selector | `String` | 
| cssText | `String` | 


### [view.js](lib/view.js) – UI helper

#### deferredIframes(placeholderSelector)
Deferred iframes: include iframes only after our own content was already loaded.
CSS class names will get copied to the new iframe element.

Example Markup:
    <div data-iframe="//www.youtube.com/embed/RsASi1EBDys" class="video"></div>

| Param | Type | Description |
| --- | --- | --- |
| placeholderSelector | `String` | The selector to find iframe placeholder tags |

#### loadIndicator(status)
Show or hide the big AJAX load icon in the middle of the screen.

| Param | Type | Description |
| --- | --- | --- |
| status | `Boolean` | Set to TRUE to show the loader and to FALSE to hide it. |

#### inlineLoadIndicator(parent, status)
Display a load indicator on a given element (e.g. a button)

| Param | Type | Description |
| --- | --- | --- |
| parent | `Element` | The element which should get a load indicator. |
| status | `Boolean` | Set to TRUE to show the loader and to FALSE to hide it. |

#### addPrintButton(target, label, cssClass)
Add a button to an element that fires `window.print()`on click.

| Param | Type | Description |
| --- | --- | --- |
| target | `String` \| `Element` | The button will get inserted after the given element. Pass in a selector or an actual element object. |
| label | `String` | The button label |
| cssClass | `String` | The button classes |


## Code snippets and helper functions for developers

* [Types of functions and notations for functions in JavaScript (ES6)](etc/function-notation.js)
* [Notation for parameter deconstruction in JavaScript (ES6)](etc/deconstructing.js)
* [Show client-side storage sizes](lib/console/getClientSideStorageSize.js) – A code snippet for displaying the currently used sizes of the localStorage

## Playground

* [AnalogClock](playground/analogclock.htm) – A clock using HTML elements and CSS transforms to draw the clock.
