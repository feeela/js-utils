# TabAccordion

A semantic and responsive CSS and JavaScript accordion, that changes its display to tabbed content based on the viewport width.

## Features

- Displays an accordion, tabbed content or plain HTML sections based on the configuration.
- Switches modes when another CSS breakpoint from your stylesheet was reached.
- Can be used with nearly any markup.
- Open sections with an ID attribute via a matching URL hash.
- Can be used with Twitter Bootstrap or Zurb Foundation stylesheets. Or you create your own styles based on the [basic standalone styles](tabaccordion.css).

## Getting started

The TabAccordion module requires the [Media Query Sync](../../lib/view.media-query-sync.js) module and [styles](../../lib/view.media-query-sync.css) to update itself, when another CSS breakpoint matches.

Use `npm install` and `npm run build` to create a `/dist/view.tabaccordion.js` that includes all dependencies.

The following documentation refers to the usage of TabAccordion as ES6 module.
You can transpile the module using [rollup.js](https://rollupjs.org/) to one of the types: `amd, cjs, es, iife, umd, system`. See [rollup.config.js](../../rollup.config.js) for `output` examples.

## Usage

### Markup

Example markup for the default configuration and [styles](./tabaccordion.css):
```html
<div data-tabaccordion class="tab-accordion-group">
    <section>
        <header>
            <h2 id="content1">Content 1</h2>
        </header>

        <p>Lorem 1 ipsum dolor sit amet, consectetur adipisicing elit. Aliquam autem doloribus error, eveniet excepturi illo in ipsam modi, nostrum quae quasi quidem ratione totam? Dignissimos distinctio iure nobis quisquam repudiandae!</p>
    </section>

    <section>
        <header>
            <h2 id="content2">Content 2</h2>
        </header>

        <p>Lorem 2 ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci architecto consequuntur dolorem laborum neque possimus unde voluptate. Adipisci consectetur esse exercitationem harum maxime, nemo qui quisquam suscipit. Facere, id?</p>
    </section>
</div>
```

### TabAccordion.setup()

Call the `TabAccordion.setup()` method to setup one or many TabAccordions in the current document.
The module does not observe any changes to the DOM after `TabAccordion.setup()` was executed.

```javascript
import * as TabAccordion from "./view.tabaccordion.js";
TabAccordion.setup();
```

The `setup` method retrieves a configuration object (see below), which it also passes to the Accordion constructor.
Two of those settings are specific to the `setup` function:

* `config.selector.wrapper` – the selector for the whole TabAccordion container element
* `config.selector.section` – the selector to find each section, consisting of a header and body, inside the wrapper

The `setup` method also sets `config.behavior` if `data-tabaccordion-behavior-type` attribute was found on the wrapper element.
This is an example of how to use different behavior types for multiple distinct TabAccordions
on one page.

### TabAccordion.Accordion

`TabAccordion.Accordion` is a public reference to the `Accordion` class. This can be used to manually create a single TabAccordion instance. See the source code of the `setup` method on how to use `new TabAccordion.Accordion()`.

```javascript
/**
 * The Accordion object
 *
 * @param {Element} wrapper The wrapper element around the tab-accordion (which already must exist).
 * @param {NodeList} sections The single sections/item of an accordion.
 * @param {Object} userConfig An object with all configuration parameters.
 *
 * @constructor
 */
Accordion( wrapper, sections, type, userConfig ) { … }
```

## Configuration

By default the TabAccordion is displayed as accordion on small screens and as tabbed content on larger screens (depending on the breakpoint and type definitions)
and opens the first section on load.
This can be controlled through the following configuration options and also some data-attributes.


### `selector` _`{Object}`_

The selector entries must contain valid CSS selectors to be used in `querySelector()` or `querySelectorAll()`.

**`selector.wrapper`** _`{String}`_

A CSS selector to identify a container that should be used as an accordion or as tabbed content.

* Should yield: Element
* Default: `[data-tabaccordion]`

**`selector.section`** _`{String}`_

This is a single part/section of the TabAccordion.
The attribute `data-tabaccordion-section` is set during initialization to identify existing sections and prevent duplicated wrapping of sections of nested TabAccordions.

* Constraint: must be descendants of `selector.wrapper`
* Should yield: NodeList
* Default: `section:not([data-tabaccordion-section])`

**`selector.sectionHeading`** _`{String}`_

This is the section header – the part which to click on to open a section.

* Constraint: must be a descendant of `selector.section`
* Should yield: Element
* Default: `:scope > header`


**`selector.sectionBody`** _`{String}`_

This is a selector to find section contents.
You may also set a single, specific body wrapper element selector here.

* Constraint: must be descendants of `selector.section`
* Should yield: NodeList
* Default: `:scope > *:not(header)`


### `behavior` _`{String|Object}`_

This setting defines the behavior of the plugin in means of, when the TabAccordion is display as accordion and when as tabbed content.

There are four predefined behavior types, which are passed as string:

- `accordion` Display an accordion, no matter what the screen size is.
- `tabAccordion` Display an accordion on small screens and tabbed content on wider screens. This is the **default**.
- `mobileOnlyAccordion` Display the content as simple panels (without using any JS), but let the collapse to an accordion on small screens.
- `tabs` Display tabbed content no matter what the screen size is.

##### Setting the behavior

1. Use the `data-tabaccordion-behavior-type` attribute, as provided by the `setup()` function. (see below)
2. Use the same type for all instances via the config-object passed to `TabAccordion.setup()`.
3. Specify distinct `config.behavior` for each instance of a (manually created) `new TabAccordion()`.

##### Defining the behavior

To use your own behavior type definitions in the data-attribute, you have to expand the list of available types in `TabAccordion.BEHAVIOR_TYPES`.
For method 2 & 3 you'll pass an object into `config.behavior`.
In both cases types follow this pattern: `{ type: { breakpoint: display } }`

- `type` is a chosen identifier to be set in the data-attribute (only used when expanding `TabAccordion.BEHAVIOR_TYPES`).
- `breakpoint` is one of the breakpoints defined in the Media Query Sync module
- `display` is one of the `TabAccordion.DISPLAY` constants. Those are `TabAccordion.DISPLAY.accordion`, `TabAccordion.DISPLAY.tabs`, `TabAccordion.DISPLAY.none` (no special behavior – just sections/panels below each other).

```javascript
TabAccordion.setup( {
    behavior: {
        xxs: TabAccordion.DISPLAY.tabs,
        xs: TabAccordion.DISPLAY.tabs,
        sm: TabAccordion.DISPLAY.tabs,
        md: TabAccordion.DISPLAY.accordion,
        lg: TabAccordion.DISPLAY.accordion,
        xl: TabAccordion.DISPLAY.accordion
    }
} );

// or

TabAccordion.BEHAVIOR_TYPES.inverted = {
    xxs: TabAccordion.DISPLAY.tabs,
    xs: TabAccordion.DISPLAY.tabs,
    sm: TabAccordion.DISPLAY.tabs,
    md: TabAccordion.DISPLAY.accordion,
    lg: TabAccordion.DISPLAY.accordion,
    xl: TabAccordion.DISPLAY.accordion
};
TabAccordion.setup();
// usage: <div data-tabaccordion data-tabaccordion-behavior-type="inverted"></div>
```

### CSS class names

**`showSectionClass`** _`{String}`_

A class name to designate opened sections. This will get added to opened section elements.
**Default:** `open`.

**`sectionBodyWrapperClass`** _`{String}`_

This class name will be added to a new element that is inserted around the section body elements.
**Default:** `tabaccordion-body-wrapper`.


**`wrapperTypeClass.accordion`**

A class name to be added to the wrapper when the TabAccordion is displayed as accordion.
**Default:** `accordion`

**`wrapperTypeClass.tabs`**

A class name to be added to the wrapper when the TabAccordion is displayed as tabbed content.
**Default:** `tabbed-content`

### Open sections

A section usually is opened via a click on the section header.
By default the first section will also open on load.
Besides that, a section may be opened on load the following ways:
 
1. Use an **URL hash matching an ID** on a section or on a section header heading.
e.g. `my-page.htm#content-A` will open a `<section id="content-A">` or a `<section><header><h2 id="content-A">` (works for hall six heading sizes).
2. Use the **`data-tabaccordion-open-section` attribute** on the wrapper element with an one-based index as value. e.g. `<div data-tabaccordion data-tabaccordion-open-section="2">`.
Use `data-tabaccordion-open-section="none"` for an accordion with no content opened on load. As tabbed content, always one section will be visible nonetheless.
3. **Overwrite `config.getInitinalSectionToOpen`** with an own function.
The return value of this function must be either a zero-based index or an actual
section element. If a section ID already matches the URL hash on page load,
this function is replaced with the index-value of that section.
Please be aware, that the data-attribute behavior from method #2 is implemented in the default
`config.getInitinalSectionToOpen()` and will not be available when overwriting this function.

Additionally  **`Accordion.open()` can be used** to open a section (using a zero-based index) **after loading** everything.
Example: Open the fourth section in the first TabAccordion on the current page.
```javascript
    let acc = TabAccordion.setup();
    acc[0].open(3);
```

To open a section after load **from outside the TabAccordion and without any references** to a `TabAccordion.Accordion` instance,
one could **update the URL hash** to another section ID. e.g. `location = location + "#content-A"` will open the `<section id ="content-A">`,
no matter from which scope the location was changed.

### data-attributes

There are two data-attributes supported on the wrapper element to control the TabAccordion via the markup.
(e.g. for several different includes)

**`data-tabaccordion-open-section`**

This option can be used to open another section than the first one (which is active/open per default). Specify a one-based index to open that section on initialization.

Set to `none` to avoid opening any sections on load.

This attribute is provided through the` TabAccordion.DEFAULT_CONFIG.getInitinalSectionToOpen()` function and thus may
not be available, if that setting was overwritten.

**`data-tabaccordion-behavior-type`**

Defines the TabAccordion behavior – i.e. whether it is displayed as accordion or tabbed content. See option `behavior` above.

This attribute is provided by the `TabAccordion.setup()` function (which sets `config.behavior`)
and thus only available when using the `setup` function.

## Examples

```html
<!-- Open 2nd section on load -->
<div data-tabaccordion data-tabaccordion-open-section="2" class="tab-accordion-group"></div>

<!-- Open no section on load -->
<div data-tabaccordion data-tabaccordion-open-section="none" class="tab-accordion-group"></div>

<!-- Always display as tabbed content at any screen size and initially open the 2nd section -->
<div data-tabaccordion data-tabaccordion-behavior-type="tabs" data-tabaccordion-open-section="2" class="tab-accordion-group"></div>
```
