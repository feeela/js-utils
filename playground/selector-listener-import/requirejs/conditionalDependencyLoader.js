/**
 *
 * @author    Thomas Heuer <technik@thomas-heuer.eu>
 * @copyright Copyright © 2021 Thomas Heuer
 */
define(function () {

    var stylesheet;

    var selectors = {};

    var module = {
        /**
         *
         * @param selector
         * @param cssText
         */
        importStyles: function (selector, cssText) {
            // Create a separate style element for use with JavaScript
            if (!stylesheet) {
                var styleNode = document.getElementsByTagName("head")[0].appendChild(document.createElement("style"));
                stylesheet = styleNode.sheet || styleNode.styleSheet;
            }

            if (typeof selector !== "string" || typeof cssText !== "string") {
                throw new Error("importStyles wrong arguments", arguments);
            }

            // insert rules one by one because of several reasons:
            // 1. IE8 does not support comma in a selector string
            // 2. if one selector fails it doesn't break others
            selector.split(",").forEach(function (selector) {
                try {
                    if (stylesheet.cssRules) {
                        stylesheet.insertRule(selector + "{" + cssText + "}", stylesheet.cssRules.length);
                    } else if (selector[0] !== "@") {
                        stylesheet.addRule(selector, cssText);
                    } else {
                        // addRule doesn't support at-rules, use cssText instead
                        stylesheet.cssText += selector + "{" + cssText + "}";
                    }
                } catch (err) {
                    // silently ignore invalid rules
                }
            });
        },

        createAnimationEventStyles: function (selector) {
            var animationID = "condeploa" + (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000);
            module.importStyles(selector, "animation-duration: 1ms !important; animation-name: " +
                animationID + " !important;");
            module.importStyles("@keyframes " + animationID, "from {opacity:.99} to {opacity:1}");
            return animationID;
        },

        /**
         *
         * @param selector
         * @param deps
         * @param initCallback
         * @param lazy
         */
        require: function (selector, deps, initCallback, lazy) {
            lazy = lazy || false;
            if (lazy === true) {
                var animationID = module.createAnimationEventStyles(selector);

                /**
                 * @todo set only one global event listener that looks up in a map for selectors with handler functions
                 */
                document.addEventListener("animationstart", function (event) {
                    if (event.animationName === animationID) {
                        // this is an internal event - stop it immediately
                        event.stopImmediatePropagation();

                        require(deps, function () {
                            initCallback.apply({
                                selector: selector,
                                element: event.target
                            }, Array.prototype.slice.call(arguments));
                        });
                    }
                }, true);
            }
            else {
                var elem = document.querySelector(selector);
                if (elem instanceof Element) {
                    require(deps, function () {
                        initCallback.apply({
                            selector: selector,
                            element: elem
                        }, Array.prototype.slice.call(arguments));
                    });
                }
            }

        }
    };

    return module;

});
