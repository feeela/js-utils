# Selector Listener Import

**Conditional, dynamic dependency loading based on a CSS selector.**

`selectorListenerImport` is intended to load JS dependencies only if a given CSS selector was found in the current HTML
document or is getting inserted dynamically into the current document. This behavior can then be used to execute
initialization- / setup-scripts for content elements, for example to load a video player script only if a `VIDEO` element
was found in the document or is inserted to it (e.g. after some AJAX operation). 

The conceptual idea for these scripts is based on the
[better-dom live extensions](https://github.com/chemerisuk/better-dom/wiki/Live-extensions).

## Use cases

* Initialize/setup scripts for AJAX content
* Initialize/setup scripts for elements an author may insert anywhere
* Load only those dependencies that are required for the current document

## How does it work?

All the variants below make use of CSS animations and the [AnimationEvent](https://developer.mozilla.org/en-US/docs/Web/API/AnimationEvent).
The script creates a CSS animation with a runtime of 1ms for each selector with a distinct animation name.
As soon as an element matching this selector is inserted into the DOM, the underlying rendering engine will
execute that CSS animation. An event listener for the `animationstart`-event on `document` level then executes
the callback for the matching animation name.

## Variants in this repository

There are multiple implementations,
[lib/dom.selector-listener-import.js](../../lib/dom.selector-listener-import.js)
(single event listener) being the most elaborate one.

### RequireJS Version

* module: [requirejs/conditionalDependencyLoader.js](requirejs/conditionalDependencyLoader.js)
* format: AMD
* handler: callback function

This variant uses the AMD module format and RequireJS to load dependencies. The AMD module
`conditionalDependencyLoader.js` exports a `require` function,
that works similar to the RequireJS `require`, but has an additional first parameter `selector`.

### ES6 import using Promises

* module: [selector-listener-import-promise.js](selector-listener-import-promise.js)
* format: ES6
* handler: Promise

This version uses ES6 imports and returns a Promise to handle the initialization script.
The **drawback** here is, that each handler is **only invoked once for each selector**.

### ES6 import using a callback (multiple event listener)

* module: [selector-listener-import-multiple-event-listener.js](selector-listener-import-multiple-event-listener.js)
* format: ES6
* handler: callback function

This version uses ES6 imports and a callback function. It registers a distinct event listener for `animationstart`
for each call to `selectorListenerImport`.

### ES6 import using a callback (single event listener)

* module: [lib/dom.selector-listener-import.js](../../lib/dom.selector-listener-import.js)
* format: ES6
* handler: callback function

This version uses ES6 imports and a callback function. It registers a single event listener to `animationstart`.
A call to `selectorListenerImport` merely adds the selector, dependency list and callback to a map,
which is used in the event handler.
