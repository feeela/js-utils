/**
 * selectorListenerImport is a conditional import
 * to load modules based on a CSS selector.
 *
 * This enables modules to be loaded automagically when a matching element
 * is inserted to the page, e.g. after an AJAX operation.
 *
 * The module exports the function `selectorListenerImport(selector, deps, callback)`.
 * The callback retrieves at least two properties: `selector` (arg[0], the input selector as refrence)
 * and `element` (arg[1], the found element), followed by the dependencies
 * in the order of the input array. The callback is executed each time
 * an element that matches the selector was found in the page.
 *
 * Example:
    import selectorListenerImport from "./selector-listener-import.js";

    selectorListenerImport(
        ".some-element",
        ['./depA.js', './depB.js'],
        (selector, element, depA, depB) => {
            console.log(selector, element, depA, depB);
        });
 *
 *
 * @author    Thomas Heuer <technik@thomas-heuer.eu>
 * @copyright Copyright © 2021 Thomas Heuer
 * @licence   MIT License
 */

/**
 * @type {CSSStyleSheet} A separate stylesheet for use with JS
 */
let stylesheet;

/**
 * Import stylesheets into the current document. Call once for each block.
 *
 * @private
 * @param {String} selector
 * @param {String} cssText
 */
function importStyles(selector, cssText) {
    // Create a separate style element for use with JavaScript
    if (!stylesheet) {
        let styleNode = document.getElementsByTagName("head")[0].appendChild(document.createElement("style"));
        stylesheet = styleNode.sheet || styleNode.styleSheet;
    }

    if (typeof selector !== "string" || typeof cssText !== "string") {
        throw new Error("importStyles wrong arguments", arguments);
    }

    // insert rules one by one because of several reasons:
    // 1. IE8 does not support comma in a selector string
    // 2. if one selector fails it doesn't break others
    selector.split(",").forEach(function (selector) {
        try {
            if (stylesheet.cssRules) {
                stylesheet.insertRule(selector + "{" + cssText + "}", stylesheet.cssRules.length);
            } else if (selector[0] !== "@") {
                stylesheet.addRule(selector, cssText);
            } else {
                // addRule doesn't support at-rules, use cssText instead
                stylesheet.cssText += selector + "{" + cssText + "}";
            }
        } catch (err) {
            // silently ignore invalid rules
        }
    });
}

/**
 * Create the animation styles for the given CSS selector and return the animation ID.
 *
 * @private
 * @param {String} selector
 * @return {string}
 */
function createAnimationEventStyles(selector) {
    let animationID = "condeploa" + (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000);
    importStyles(selector, "animation-duration: 1ms !important; animation-name: " +
        animationID + " !important;");
    importStyles("@keyframes " + animationID, "from {opacity:.99} to {opacity:1}");
    return animationID;
}

/**
 * @param {String} selector The CSS selector to listen to
 * @param {Array} deps Dependencies as array of string
 * @param {Function} callback This function uses a callback instead of being based on a Promise, to allow multiple executions
 * @return {Promise} The Promise resolves with an array as return value; use destructuring to provide the single dependencies inside your Promise resolver
 */
function selectorListenerImport(selector, deps, callback) {

    // Create the animation styles, on which the event listener is listening
    let animationID = createAnimationEventStyles(selector);

    // Listen to CSS animation-start events
    document.addEventListener("animationstart", function (event) {
        if (event.animationName === animationID) {
            // This is an internal event - stop it immediately
            event.stopImmediatePropagation();

            // Turn string list of dependencies into an array of Promises
            let dependencyPromises = deps.map(dep => {
                let depProm;
                depProm = import(dep).catch(importError => reject(importError));
                return depProm;
            });

            // Wait until all modules are loaded and finally resolve the Promise
            Promise.all(dependencyPromises)
                .then(modules => {
                    callback.apply(null, [selector, event.target, ...modules]);
                });
        }
    }, true);
}

export {selectorListenerImport as default};
