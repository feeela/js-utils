/**
 * This files demonstrates the notation for parameter deconstruction in JS
 */

/* array parameter deconstruction */
function f1([a, b]) {
    console.log("f1", a, b);
}

f1([1, 2, 3]);

/* skip array values when deconstructing */
function f2([,,,x,y]) {
    console.log("f2", x, y);
}

f2([1, 2, 3, 4, 5]);

/* simple object parameter deconstruction */
let person = {
    id: 1,
    givenName: "Thomas",
    familyName: "Heuer"
};

function f3({givenName, familyName}) {
    console.log("f3", givenName, familyName);
}

f3(person);


/* object parameter deconstruction with renaming */
function f4a({id: userId}) {
    console.log("f4a", userId);
}

function f4b({givenName: first, familyName: last}) {
    console.log("f4b", first + " " + last);
}

f4a(person);
f4b(person);

/* object parameter deconstruction with multiple levels, example 1 */
let data = {
    id: 1,
    type: String,
    content: "Hallo Welt",
    user: person
};

function f5({user: {id: userId}}) {
    console.log("f5", userId);
}

f5(data);

/* parameter deconstruction with multiple levels, example 2 */
const config = {
    view: {
        screen: {
            resolution: {
                x: 1600,
                y: 900
            },
            colorDepth: 64
        }
    }
};

function f6({view: {screen: {colorDepth}}}) {
    console.log("f6", colorDepth);
}

f6(config);

/* parameter deconstruction with multiple levels, renaming and default value */
function f7({view: {screen: {colorDepth: cd = 32}}}) {
    console.log("f7", cd);
}

delete config.view.screen.colorDepth;
f7(config);

/* deconstruction with utility function as default value */
function getDefaultColorDepth() {
    return Math.pow(2, 7); // 128
}

function f8({view: {screen: {colorDepth = getDefaultColorDepth()}}}) {
    console.log("f8", colorDepth);
}

f8(config);

/* Arrow function inside IIFE pattern with parameter deconstruction,
 * rest parameter and using modulo operator.
 * Yes, this is valid, but unreadable JavaScript.
 */
(({a: x = 1, b: y = 2}, ...z) => x * y % z[0])({a: 3}, 4);
// 2
