/**
 * This file demonstrates the different types of functions and notations for functions in JavaScript (ES6)
 */


/* ****************************** */
/* function declaration             */
/* ****************************** */

/*
 * function declarations are hoisted,
 * that is why the following line would work even before declaration
 */
console.log("declaA", declaA(1, 2));

function declaA(foo, bar) {

    return foo + bar;
}

/* default parameter value */
function declaB(foo = 1, bar = 2) {
    return foo + bar;
}
console.log("declaB", declaB());

/* rest parameter */
function declaC(foo, bar, ...restParams) {
    return [foo, bar, restParams];
}
console.log("declaC", declaC(1, 2, 3, 4, 5));

/* destructuring of array parameters */
function declaD([foo, bar]) {
    return foo + bar;
}
console.log("declaD", declaD([1, 2]));

/* destructuring of object parameters */
function declaE({foo, bar}) {
    return foo + bar;
}
console.log("declaE", declaE({foo: 1, bar: 2}));

/* destructuring of object parameters and renaming */
function declaF({foo: foz, bar: baz}) {
    return foz + baz;
}
console.log("declaF", declaF({foo: 1, bar: 2}));

/* destructuring of object parameters and renaming and default values */
function declaG({foo: foz = 1, bar: baz = 2}) {
    return foz + baz;
}
console.log("declaG", declaG({}));
// calling the function with no parameter will yield "Uncaught TypeError: Cannot read property 'foo' of undefined"
// console.log( "declaG", declaG() );


/* ****************************** */
/* function expression            */
/* ****************************** */

/*
 * function expressions are not hoisted
 *   console.log( "exprA", exprA(1, 2) );
 * at this point this line would lead to "TypeError: exprA is not a function"
 */

const exprA = function (foo, bar) {
    return foo + bar;
};
console.log("exprA", exprA(1, 2));

/* function expression with named function */
const exprB = function exprBName(foo, bar) {
    return foo + bar;
};
// console.log( "exprBName(1, 2)", exprBName(1, 2) );
// the name ist only visible inside the function (i.e. for recursions)
// so this line would yield an "Uncaught ReferenceError: exprBName is not defined"
console.log("exprB", exprB(1, 2));


/* ****************************** */
/* arrow function expression      */
/* ****************************** */

const arrowA = (foo, bar) => {
    return foo + bar;
};
console.log("arrowA", arrowA(1, 2));

const arrowB = (foo, bar) => foo + bar;
console.log("arrowB", arrowB(1, 2));

const arrowC = foo => foo + 1;
console.log("arrowC", arrowC(1));

/* default parameter value */
const arrowD = (foo = 1, bar = 2) => foo + bar;
console.log("arrowD", arrowD());

/* rest parameters */
const arrowE = (foo, bar, ...restParams) => [foo, bar, restParams];
console.log("arrowE", arrowE(1, 2, 3, 4, 5));

/* returning object literal: wrapped in parentheses */
const arrowF = (foo, bar) => ({f: foo, b: bar});
console.log("arrowF", arrowF(1, 2));

/* destructuring of parameters */
const arrowG = ([foo, bar]) => foo + bar;
console.log("arrowG", arrowG([1, 2]));

/* destructuring of parameters */
const arrowH = ({foo: foz, bar: baz}) => foz + baz;
console.log("arrowH", arrowH({foo: 1, bar: 2}));


/* ****************************** */
/* method notation                */
/* ****************************** */

/* a shorthand syntax for object methods, works wit generators and async functions too */
const objectD = {
    foobar: undefined,
    foo(foobar) {
        this.foobar = foobar;
    },
    bar() {
        return this.foobar;
    }
};
console.log("objectD.", objectD);


/* ****************************** */
/* object getter & setter         */
/* ****************************** */

/* define getter & setter in literal notation (on prototype) */
const objectA = {
    bar: null,
    get foo() {
        return this.bar;
    },
    set foo(fooValue) {
        this.bar = fooValue;
    }
};
objectA.foo = 42;
console.log("objectA.foo", objectA.foo);

/* with variable/computed property name */
let propKey = "foo";
const objectB = {
    bar: null,
    get [propKey]() {
        return this.bar;
    },
    set [propKey](fooValue) {
        this.bar = fooValue;
    }
};
objectB.foo = 5;
console.log("objectB.foo", objectB.foo);
// or
// objectB[propKey] = 5;
// console.log("objectB[propKey]", objectB[propKey]);

/* define getter & setter on existing object (on instance) */
const objectC = {bar: null};
Object.defineProperty(objectB, "foo", {
    get: function () {
        return this.bar;
    },
    set: function (fooValue) {
        this.bar = fooValue;
    }
});
objectC.foo = 23;
console.log("objectC.foo", objectC.foo);


/* lazy loading of properties using a getter */
const objectE = {
    get foo() {
        delete this.foo;
        return this.foo = "Now foo has a static value";
    }
};
console.log("objectE", objectE);
console.log("objectE.foo", objectE.foo);
console.log("objectE", objectE);


/* ****************************** */
/* async functions                */
/* ****************************** */

/*
 * - each "async" is like "return new Promise()"
 * - each "await" is like a .then()
 * - .catch() is replaced by "try { await … } catch { }"
 */

/**
 * Helper function, that returns a Promise, which is resolved after N seconds.
 * @param {Number} seconds
 * @return {Promise}
 */
function resolveAfterNSeconds(seconds) {
    return new Promise(resolve => {
        setTimeout(() => resolve(42), seconds * 1000);
    });
}

/**
 * Helper function, that returns a Promise, which is rejected after N seconds.
 * @param {Number} seconds
 * @return {Promise}
 */
function rejectAfterNSeconds(seconds) {
    return new Promise((resolve, reject) => {
        setTimeout(() => reject("That didn't work."), seconds * 1000);
    });
}

async function asyncA() {
    return await resolveAfterNSeconds(2);
}

/* use the IIFE to use await on top level */
(async () => {
    let answer = await asyncA();
    console.log("asyncA", answer);
})();


/* reject a promise */
async function asyncB() {
    "use strict";
    try {
        return await rejectAfterNSeconds(1);
    } catch (error) {
        console.error(error);
        return null;
    }
}
asyncB();

/* parallel execution of multiple await's */
async function asyncC() {
    /* both timers start immediately */
    let slow = resolveAfterNSeconds(3);
    let fast = resolveAfterNSeconds(1);

    console.log("asyncC.slow", await slow);
    /* this is invoked immediately, since fast was already resolved during runtime of slow */
    console.log("asyncC.fast", await fast);
}
asyncC();


/* ****************************** */
/* generator functions            */
/* ****************************** */

/*
 * Generator functions always return an iterable with a function .next()
 * that returns an object {value: …, done: …} on each call;
 * A generator function is executed until the first yield statement, returns its
 * value and pauses, until it is invoked again, to resume until the next yield
 * statement;
 */

/* multiple yield statements */
function* generatorA() {
    yield 5;
    yield 6;
    yield 7;
    yield 8;
    yield 9;
}

let genA = generatorA();
console.log("generatorA",
    genA.next().value,
    genA.next().value,
    genA.next().value,
    genA.next().value,
    genA.next().value
);

/* yield in a loop */
function* generatorB() {
    let index = 0;
    while (true)
        yield index++;
}

let genB = generatorB();
console.log("generatorB",
    genB.next().value,
    genB.next().value,
    genB.next().value,
    genB.next().value,
    genB.next().value
);

/* a return statement marks the generator as finished */
function* generatorC() {
    yield 1;
    return 2;
    yield 3;
}
let genC = generatorC();
console.log("generatorC",
    genC.next(),
    genC.next(),
    genC.next()
);

/**
 * List powers of {power} until {stop} using a generator function
 * @param {Number} stop
 * @param {Number} power
 */
function sq(stop = 100, power = 2) {
    /**
     * @param {Number} n
     */
    function* powers(n) {
        // endless loop to generate
        for (let current = n; ; current *= n) {
            yield current;
        }
    }

    for (let currentPower of powers(power)) {
        // controlling generator
        if (currentPower > stop) break;
        console.log(currentPower)
    }
}
sq(512);


/* ****************************** */
/* Function constructor           */
/* ****************************** */

/*
 * - out-of-scope: can only access global and own variables, not the ones from the defining scope
 * - separately parsed when defined
 */

const constructorA = new Function("foo", "bar", "return foo + bar;");
console.log("constructorA:", constructorA(1, 2));

const constructorB = Function("foo", "bar", "return foo + bar;");
console.log("constructorB:", constructorB(1, 2));
