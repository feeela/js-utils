/**
 * Configuration for Rollup
 *
 * @link https://github.com/rollup/rollup
 *
 * For all options see:
 * @link https://rollupjs.org/guide/en/#big-list-of-options
 */

import {nodeResolve} from '@rollup/plugin-node-resolve';

export default {
    // Add extra entry points here if there are multiple to build
    input: [
        'lib/view.tabaccordion.js',
        'lib/dom.selector-listener-import.js',
    ],

    output: [
        // ES module build
        {
            dir: 'dist',
            format: 'esm',
            compact: true
        }
        ,
        // AMD module build
        {
            dir: 'dist/amd',
            format: 'amd',
            compact: true
        }
    ],

    plugins: [
        nodeResolve()
    ],

    // disable external module warnings
    // (JSPM / the import map handles these for us instead)
    onwarn (warning, warn) {
        if (warning.code === 'UNRESOLVED_IMPORT')
            return;
        warn(warning);
    }
};
